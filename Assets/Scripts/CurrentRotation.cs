﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentRotation : MonoBehaviour
{
    public FixedJoystick joystick;
    private Vector3 lastRotation;


    private void Start()
    {
        lastRotation = lastRotation = new Vector3(this.transform.localRotation.x, 90f, -67f);
    }


    void Update()
    {
        UpdateRotation();
    }

    private void UpdateRotation()
    {
        if (joystick.Horizontal > 0.3)
        {
            lastRotation = new Vector3(this.transform.localRotation.x, 180f, -67f);
        }
        else if (joystick.Horizontal < -0.3)
        {
            lastRotation = new Vector3(this.transform.localRotation.x, 0, -67f);
        }
        else if (joystick.Vertical > 0.3)
        {
            lastRotation = new Vector3(this.transform.localRotation.x, 90f, -67f);
        }
        else if (joystick.Vertical < -0.3)
        {
            lastRotation = new Vector3(this.transform.localRotation.x, 270f, -67f);
        }
        this.transform.DOLocalRotate(lastRotation, 0.5f);
    }


    }

