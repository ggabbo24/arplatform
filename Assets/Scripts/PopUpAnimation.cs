﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PopUpAnimation : MonoBehaviour
{
    
    private void PlayAnimation()
    {
        EnterAnim();
        Invoke("ExitAnim", 4f);
    }

    public void EnterAnim()
    {
        this.transform.DOMoveX(1860f, 2);
       
    }
    public void ExitAnim()
    {
        this.transform.DOMoveX(1975f, 2);
    }
}
