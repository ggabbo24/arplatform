﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject gameOverUI;
    public GameObject endLevelUI;
    public Transform spawnPoint;
    public GameObject joystick;
    public GameObject jumpButton;
    public GameObject player;
    public GameObject optionsBar;
    public Image[] keys;
    public Sprite[] keySprites;
    public GameObject[] movingPlatforms;
    
    public GameObject EndLevel;

    private bool playing = false;
    
    public void StartGame()
    {
        if (!playing)
        {
            playing = true;
            joystick.gameObject.SetActive(true);
            jumpButton.gameObject.SetActive(true);
            player.gameObject.SetActive(true);
            for (int i = 0;i < movingPlatforms.Length; i++)
            {
                movingPlatforms[i].GetComponent<PlatformMov>().enabled = true;
            }
           
        }
        else
        {
            playing = false;
            joystick.gameObject.SetActive(false);
            jumpButton.gameObject.SetActive(false);
            player.gameObject.SetActive(false);
            for (int i = 0; i < movingPlatforms.Length; i++)
            {
                movingPlatforms[i].GetComponent<PlatformMov>().enabled = false;
            }
        }
    }


    private void LevelCompleted()
    {
        player.gameObject.SetActive(false);
        endLevelUI.gameObject.SetActive(true);
        joystick.gameObject.SetActive(false);
        jumpButton.gameObject.SetActive(false);
        optionsBar.gameObject.SetActive(false);
        
    }

    private void GameOver()
    {
        player.gameObject.SetActive(false);
        gameOverUI.gameObject.SetActive(true);
        joystick.gameObject.SetActive(false);
        jumpButton.gameObject.SetActive(false);
        optionsBar.gameObject.SetActive(false);

    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void GotKey(int keyNum)
    {
        keys[keyNum].sprite = keySprites[1];
        keys[keyNum].transform.DOShakeRotation(1f);
        if (keyNum >= keys.Length - 1)
        {
            EndLevel.SetActive(true);
        }
        
    }

    public void Retry(bool daCheckpoint)
    {
        if (daCheckpoint) 
        { 
            player.transform.position = player.gameObject.GetComponent<PlayerMov>().lastCheckpoint.position;
        }
        else 
        { 
            player.transform.position = player.gameObject.GetComponent<PlayerMov>().startPos.position ;
            player.GetComponent<PlayerMov>().ResetCheckpoint();
        }

        joystick.gameObject.SetActive(true);
        joystick.GetComponent<FixedJoystick>().resetJoystick();
        jumpButton.gameObject.SetActive(true);
        player.gameObject.SetActive(true);
        optionsBar.gameObject.SetActive(true);
    }
    
}
