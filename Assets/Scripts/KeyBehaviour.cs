﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBehaviour : MonoBehaviour
{
    public int keyNum;

    private void Start()
    {
        transform.DOLocalRotate(new Vector3(0, 360, 0), 2f).SetLoops(-1, LoopType.Incremental);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            
            this.gameObject.SetActive(false);
            GameObject.Find("GameManager").SendMessage("GotKey", keyNum);
        }
    }
}
