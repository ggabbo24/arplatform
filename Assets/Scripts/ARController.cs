﻿
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARController : MonoBehaviour
{
    // DEBUG VARS
    // #################################################
    //[SerializeField] TMPro.TMP_Text StateText;
    //[SerializeField] ARPlaneManager planeManager;
    // [SerializeField] ARPointCloudManager cloudManager;
    //private int currentPlaneNum = 0;
    //private int currentPointNum = 0;
    // #################################################

    [SerializeField] ARRaycastManager raycastManager;
    [SerializeField] Transform level;
    [SerializeField] ARCameraManager cameraManager;
    [SerializeField] Light dirLight;

    public GameObject zoomImg;
    public GameObject rotateImg;
    public GameObject playImg;
    public GameObject pauseImg;
    public Transform placementIndicator;
    public Button placeBTN;
    public Button retryBTN;
    public Button restartBTN;
   
    private Touch touch;
    
    public Boolean place = true;
    public Boolean rotate = true;
    

    private float avgBright;
    private float avgColor;
    private Color colorCorr;
    private Vector3 startGrav;

    private void Start()
    {

        //ARSession.stateChanged += UpdateStateText;
        placementIndicator.gameObject.SetActive(false);
        level.gameObject.SetActive(false);
        retryBTN.interactable = false;
        restartBTN.interactable = false;
        cameraManager.frameReceived += LightEstimation;
    }

    private void Update()
    {

        var hits = new List<ARRaycastHit>();
        raycastManager.Raycast(cameraManager.gameObject.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)), hits, TrackableType.Planes);

        if (hits.Count > 0)
        {

            placementIndicator.position = hits[0].pose.position;
            
            if (!placementIndicator.gameObject.activeInHierarchy && !level.gameObject.activeInHierarchy)
            {
                placementIndicator.gameObject.SetActive(true);
            }

            if (Input.touchCount == 1 && place && !IsPointerOverUIObject())
            {
                touch = Input.GetTouch(0);

                if (!level.gameObject.activeInHierarchy)
                {
                    placementIndicator.gameObject.SetActive(false);
                    level.position = hits[0].pose.position;
                    level.gameObject.SetActive(true);
                }
            }

            if (Input.touchCount >= 2 && place)
            {
                var pos1 = Input.GetTouch(0).position;
                var pos2 = Input.GetTouch(1).position;
                var pos1b = Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition;
                var pos2b = Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition;

                if (!rotate)
                {
                    var scale = Vector3.Distance(pos1, pos2) /
                               Vector3.Distance(pos1b, pos2b);

                    if (scale == 0 || scale > 10)
                        return;

                    level.transform.localScale = new Vector3(Math.Min(Math.Max(level.transform.localScale.x * scale, 1f),2f),Math.Min(Math.Max(level.transform.localScale.y * scale, 0.2f),0.4f), Math.Min(Math.Max(level.transform.localScale.z * scale, 1f),2f));
                    
                }
                else
                {
                    var angle = Vector2.SignedAngle(pos2 - pos1, pos2b - pos1b);

                    level.transform.RotateAround(level.position, Vector3.up, angle);
                }
            }
        }
        dirLight.intensity = avgBright + 1f;
        dirLight.colorTemperature = avgColor;
        dirLight.color = colorCorr;

        
        
    }

    private void LightEstimation(ARCameraFrameEventArgs cameraFrame)
    {
       if (cameraFrame.lightEstimation.averageBrightness != null)
            avgBright = cameraFrame.lightEstimation.averageBrightness.Value;
       if (cameraFrame.lightEstimation.averageColorTemperature != null)
            avgColor = cameraFrame.lightEstimation.averageColorTemperature.Value;
       if (cameraFrame.lightEstimation.colorCorrection != null)
            colorCorr = cameraFrame.lightEstimation.colorCorrection.Value;
    }
    
    public void rotateButton()
    {
        if (rotate == true)
        {
            rotate = false;

            zoomImg.SetActive(false);
            rotateImg.SetActive(true);
        }
        else
        {
            rotate = true;
            zoomImg.SetActive(true);
            rotateImg.SetActive(false);
        }
    }
    public void placeButton()
    {
        level.gameObject.SetActive(false);
        placementIndicator.gameObject.SetActive(true);
    }

    public void playButton()
    {
        if (place)
        {
            place = false;
            playImg.SetActive(false);
            pauseImg.SetActive(true);
            placeBTN.gameObject.SetActive(false);
            retryBTN.interactable = true;
            restartBTN.interactable = true;
        }
        else
        {
            place = true;
            playImg.SetActive(true);
            pauseImg.SetActive(false);
            placeBTN.gameObject.SetActive(true);
            retryBTN.interactable = false;
            restartBTN.interactable = false;
        }
        
    }

   

    //private void UpdateStateText(ARSessionStateChangedEventArgs stato)
    //{
    //    StateText.text = stato.state.ToString();
    //}

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
