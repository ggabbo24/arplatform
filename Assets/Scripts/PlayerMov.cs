﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class PlayerMov : MonoBehaviour
{

    // public Text currentPoints;
    //  public AudioSource audioSource;
    //  public AudioClip jumpStartSound;
    //  public AudioClip jumpEndSound;

    private Rigidbody player;
    public float jumpPower;
    public float movSpeed;


    [HideInInspector] public int playerPoints = 0;
    [HideInInspector] public Boolean finishedLevel = false;

    public ARCameraManager ARcamera;
    public FixedJoystick joystick;
    public Button jumpButton;
    public Transform level;
    public int keyToCollect;
    private int keyCollected;

    public Boolean canJump = false;
    public Transform startPos;
    public Transform lastCheckpoint;
    public Animator animator;
    private float distanceGround;
    public AudioSource jumpAudio;
    public AudioSource winAudio;
    public AudioSource dieAudio;
    public AudioSource keyAudio;

    void Start()
    {
        keyCollected = 0;
        lastCheckpoint.position = startPos.position ;
        distanceGround = GetComponent<Collider>().bounds.extents.y;
    }

    private void Awake()
    {
        player = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPostition = new Vector3(ARcamera.transform.position.x,
                                       this.transform.position.y,
                                       ARcamera.transform.position.z);
        this.transform.LookAt(2 * transform.position - targetPostition);

        

        if (Physics.Raycast(transform.position, -Vector3.up, distanceGround + 0.1f))
            canJump = true;
        else
            canJump = false;

    }

    private void FixedUpdate()
    {
        if (Math.Abs(joystick.Vertical) > 0.3f || Math.Abs(joystick.Horizontal) > 0.3f)
            transform.Translate(new Vector3(level.localScale.x* joystick.Horizontal / movSpeed, 0.0f, level.localScale.z*joystick.Vertical / movSpeed));

        animator.SetFloat("SpeedHOR", joystick.Horizontal);
        animator.SetFloat("SpeedVER", joystick.Vertical);

       
        
    }

    //NormalizedJump -> base local scale = 0.2f
    public void Jump()
    {
        if (canJump)
        {
            player.AddForce(new Vector3(0f, jumpPower + level.localScale.x, 0f), ForceMode.Impulse);
            jumpAudio.Play();
        }
    }

    public void ResetCheckpoint()
    {
        this.lastCheckpoint.position = startPos.position;
        GameObject[] checkpoints = GameObject.FindGameObjectsWithTag("Checkpoint");
        if (checkpoints.Length != 0)
            for (int i = 0; i < checkpoints.Length; i++)
            {
                checkpoints[i].SendMessage("ResetState");
            }
    }
    private void GameOver()
    {
        GameObject.Find("GameManager").SendMessage("GameOver");
    }

    private void LevelCompleted()
    {
        GameObject.Find("GameManager").SendMessage("LevelCompleted");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Lava")
        {
            animator.SetTrigger("Dead");
            dieAudio.Play();
            Invoke("GameOver",2f);
        }
       
        if (collision.collider.tag == "Checkpoint")
        {
            if (collision.collider.transform.position.x != lastCheckpoint.position.x)
            {
                Vector3 pos = collision.collider.transform.position;
                lastCheckpoint.position = new Vector3(pos.x, pos.y + 0.05f, pos.z);
                keyAudio.Play();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Key")
        {
            Vector3 pos = other.transform.position;
            lastCheckpoint.position = new Vector3(pos.x, pos.y + 0.05f, pos.z);
            keyAudio.Play();
            keyCollected++;
        }
        if (other.tag == "EndLevel")
        {
            if (keyCollected == keyToCollect)
            {
                animator.SetTrigger("Win");
                winAudio.Play();
                Invoke("LevelCompleted", 2f);
            }

        }
    }
}
