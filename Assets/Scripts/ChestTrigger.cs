﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestTrigger : MonoBehaviour
{
    public GameObject whatToTrigger;
    public int enemyType;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            whatToTrigger.GetComponent<EnemyBehaviour>().enabled = true;

        }

    }

}

