﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EnemyBehaviour : MonoBehaviour
{
    public Vector3 movement;
    public Vector3 startPos;
    public Animator enemyAnimator;
    public float animDuration;

    private void Start()
    {
        startPos = this.transform.localPosition;
        enemyAnimator.SetBool("PlayerNear", true);
        StartCoroutine(ForwardMov());
    }

    private IEnumerator ForwardMov()
    {
        enemyAnimator.SetFloat("SpeedHOR", 0.2f);
        transform.DOLocalMove(new Vector3(startPos.x+movement.x,startPos.y+movement.y,startPos.z+movement.z), animDuration);
        yield return new WaitForSeconds(animDuration);
        enemyAnimator.SetFloat("SpeedHOR", 0);
        transform.DOLocalRotate(new Vector3(0,270,0), 1f);
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(BackwardsMov());
    }
    private IEnumerator BackwardsMov()
    {
        enemyAnimator.SetFloat("SpeedHOR", 0.2f);
        transform.DOLocalMove(startPos, animDuration);
        yield return new WaitForSeconds(animDuration);
        enemyAnimator.SetFloat("SpeedHOR", 0);
        transform.DOLocalRotate(new Vector3(0, 90, 0), 1f);
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(ForwardMov());
    }
}
