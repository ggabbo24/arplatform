﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMov : MonoBehaviour
{
    public Vector3 startPos;
    public Vector3 movement;

    private void Start()
    {
        startPos = transform.localPosition;
        StartCoroutine(Behaviour());
    }

    private IEnumerator Behaviour()
    {
        transform.DOLocalMove(new Vector3(startPos.x + movement.x, startPos.y + movement.y, startPos.z + movement.z), 5f);
        yield return new WaitForSeconds(6);
        transform.DOLocalMove(startPos, 5f);
        yield return new WaitForSeconds(6);
        yield return StartCoroutine(Behaviour());
    }
    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.collider.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.collider.transform.SetParent(null);
        }
    }
}
