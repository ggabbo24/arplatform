﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slideshow : MonoBehaviour
{
    public Image[] images = new Image[3];
    private int currentImg;
    // Start is called before the first frame update

    private void Start()
    {
        images[0].gameObject.SetActive(true);
        currentImg = 0;
    }

    public void Next()
    {
        images[currentImg].gameObject.SetActive(false);
        if (currentImg + 1 >= images.Length)
        {
            currentImg = 0;
        }
        else
        {
            currentImg++;
        }
        images[currentImg].gameObject.SetActive(true);
    }
    public void Back()
    {
        images[currentImg].gameObject.SetActive(false);
        if (currentImg - 1 < 0)
        {
            currentImg = images.Length - 1;
        }
        else
        {
            currentImg--;
        }
        images[currentImg].gameObject.SetActive(true);
    }
}
