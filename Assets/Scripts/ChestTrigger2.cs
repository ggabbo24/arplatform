﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestTrigger2 : MonoBehaviour
{
    public GameObject whatToTrigger;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            whatToTrigger.GetComponent<EnemyBehaviour2>().enabled = true;

        }

    }

}

