﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class EnemyBehaviour2 : MonoBehaviour
{
  
    public Vector3 startPos;
    public Animator enemyAnimator;
    public float animDuration;

    private void Start()
    {
        startPos = this.transform.localPosition;
        enemyAnimator.SetBool("PlayerNear", true);
        StartCoroutine(CircleMov());
    }

    private IEnumerator CircleMov()
    {
        //Movimento rettilineo
        enemyAnimator.SetFloat("SpeedHOR", 0.2f);
        transform.DOLocalMove(new Vector3(transform.localPosition.x - 0.3f, startPos.y + 0, transform.localPosition.z + 0), animDuration);
        yield return new WaitForSeconds(animDuration);
        //Ruoto 90 gradi
        enemyAnimator.SetFloat("SpeedHOR", 0);
        transform.DOLocalRotate(new Vector3(0, 0, 0), 1f);
        yield return new WaitForSeconds(1);
        //Movimento rettilineo
        enemyAnimator.SetFloat("SpeedHOR", 0.2f);
        transform.DOLocalMove(new Vector3(transform.localPosition.x + 0, startPos.y + 0, transform.localPosition.z + 0.1f), 2f);
        yield return new WaitForSeconds(2);
        //Ruoto 90 gradi
        enemyAnimator.SetFloat("SpeedHOR", 0);
        transform.DOLocalRotate(new Vector3(0, 90, 0), 1f);
        yield return new WaitForSeconds(1);
        //Movimento Rettilineo
        enemyAnimator.SetFloat("SpeedHOR", 0.2f);
        transform.DOLocalMove(new Vector3(transform.localPosition.x + 0.3f, startPos.y + 0, transform.localPosition.z + 0), animDuration);
        yield return new WaitForSeconds(animDuration);
        //Ruoto 90 gradi
        enemyAnimator.SetFloat("SpeedHOR", 0);
        transform.DOLocalRotate(new Vector3(0, 180, 0), 1f);
        yield return new WaitForSeconds(1);
        //Movimento rettilineo
        enemyAnimator.SetFloat("SpeedHOR", 0.2f);
        transform.DOLocalMove(new Vector3(transform.localPosition.x + 0, startPos.y + 0, transform.localPosition.z - 0.1f), 2f);
        yield return new WaitForSeconds(2);
        //Ruoto 90 gradi
        enemyAnimator.SetFloat("SpeedHOR", 0);
        transform.DOLocalRotate(new Vector3(0, 270, 0), 1f);
        yield return new WaitForSeconds(1);

        yield return StartCoroutine(CircleMov());
    }

   
}
